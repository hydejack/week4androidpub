package cs.mad.flashcards.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.CellClickListener
import cs.mad.flashcards.activities.CellLongClickListener
import cs.mad.flashcards.databinding.ItemFlashcardBinding
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(val context: Context, dataSet: List<Flashcard>, val cellClickListener: CellClickListener, val cellLongClickListener: CellLongClickListener) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: View = view

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        val flashcardTitle = viewHolder.binding.findViewById<TextView>(R.id.flashcard_title)
        flashcardTitle.text = item.question

        viewHolder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(item, position)
        }

        viewHolder.itemView.setOnLongClickListener {
            cellLongClickListener.onCellLongClickListener(item, position)
            true
        }

//        viewHolder.binding.rootView.rootView.findViewById<MaterialButton>(R.id.study_set_button).setOnClickListener {
//            clickListener.onClickListener(viewHolder)
//        }

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }

    fun removeItem(it: Flashcard) {
        val temp = dataSet.indexOf(it)
        dataSet.remove(it)
        notifyItemRemoved(temp)
    }

    fun getList() : ArrayList<Flashcard>{
        return ArrayList(dataSet)
    }
}