package cs.mad.flashcards.activities

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.entities.Flashcard
import java.io.Serializable
import android.view.GestureDetector

class StudySetActivity : AppCompatActivity() {
    private lateinit var binding : ActivityStudySetBinding
    var good : Int = 0
    var bad : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        println("we here")
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var cardarray = (intent.getBundleExtra("cardList")?.getSerializable("cards") as ArrayList<Flashcard>)
        var cardlist = cardarray as MutableList<Flashcard>
        binding.textView1.text = cardlist[0].question

        binding.textView1.setOnClickListener {
            //flip card
            if (binding.textView1.text == cardlist[0].question.toString()) {
                binding.textView1.text = cardlist[0].answer
            }
            else {
                binding.textView1.text = cardlist[0].question
            }
        }

        binding.correct.setOnClickListener {
            //add to correct total, remove from stack
            good = good + 1
            cardlist.removeAt(0)
            binding.textView1.text = cardlist[0].question
        }

        binding.missed.setOnClickListener {
            //add to missed total, move to bottom of stack
            bad = bad + 1
            cardlist.add(cardlist[0])
            cardlist.removeAt(0)
            binding.textView1.text = cardlist[0].question
        }

        binding.skip.setOnClickListener {
            //move to bottom of stack
            cardlist.add(cardlist[0])
            cardlist.removeAt(0)
            binding.textView1.text = cardlist[0].question
        }

        binding.exit.setOnClickListener {
            finish()
        }
    }

}