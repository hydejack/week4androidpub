package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import android.content.Intent

interface CellClickListener {
    fun onCellClickListener(data : Flashcard, position : Int)
}

interface CellLongClickListener {
    fun onCellLongClickListener(data : Flashcard, position : Int)
}

//interface  ClickListener {
//    fun onClickListener(view: FlashcardAdapter.ViewHolder)
//}

class FlashcardSetDetailActivity : AppCompatActivity(), CellClickListener, CellLongClickListener {
    private lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.flashcardList.adapter =
            FlashcardAdapter(this, Flashcard.getHardcodedFlashcards(), this, this)

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard("test", "test"))
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        val i = Intent(this@FlashcardSetDetailActivity, StudySetActivity::class.java)
        var b = Bundle()
        b.putSerializable("cards", (binding.flashcardList.adapter as FlashcardAdapter).getList())
        i.putExtra("cardList", b)

        binding.studySetButton.setOnClickListener {
            binding.flashcardList.context.startActivity(i)
        }
    }

    private fun showFlashcardDialog(data: Flashcard, position: Int) {
        AlertDialog.Builder(this)
            .setTitle(data.question)
            .setMessage(data.answer)
            .setPositiveButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                showFlashcardEdit(data, position)
            }
            .setNegativeButton("Cancel") { dialogInterface: DialogInterface, i: Int ->
                dialogInterface.cancel()
            }
            .create()
            .show()
    }

    private fun showFlashcardEdit(data: Flashcard, position: Int) {
        val titleView = layoutInflater.inflate(R.layout.custom_title, null)
        val bodyView = layoutInflater.inflate(R.layout.custom_body, null)
        val titleEditText = titleView.findViewById<EditText>(R.id.custom_title)
        val bodyEditText = bodyView.findViewById<EditText>(R.id.custom_body)
        titleEditText.setText(data.question)
        bodyEditText.setText(data.answer)

        AlertDialog.Builder(this)
            .setCustomTitle(titleView)
            .setView(bodyView)
            .setPositiveButton("Done") { dialogInterface: DialogInterface, i: Int ->
                (binding.flashcardList.adapter as FlashcardAdapter).addItem(
                    Flashcard(
                        titleEditText.getText().toString(), bodyEditText.getText().toString()
                    )
                )
                (binding.flashcardList.adapter as FlashcardAdapter).removeItem(data)
            }
            .setNegativeButton("Delete") { dialogInterface: DialogInterface, i: Int ->
                (binding.flashcardList.adapter as FlashcardAdapter).removeItem(data)
                dialogInterface.cancel()
            }
            .create()
            .show()
    }

    override fun onCellClickListener(data: Flashcard, position: Int) {
        showFlashcardDialog(data, position)
    }

    override fun onCellLongClickListener(data: Flashcard, position: Int) {
        showFlashcardEdit(data, position)
    }
}