package cs.mad.flashcards.entities

import java.io.Serializable


data class Flashcard (
    val question: String,
    val answer: String
) : Serializable {

    companion object : Serializable {
        fun getHardcodedFlashcards(): List<Flashcard> {
            var cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Definition $i"))
            }
            return cards
        }
    }
}