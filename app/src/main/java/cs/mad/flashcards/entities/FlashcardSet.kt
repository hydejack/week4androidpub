package cs.mad.flashcards.entities

import java.io.Serializable

data class FlashcardSet(val title: String) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            var sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}